# Express-SQLite Application

This repository houses a simple Express.js application seamlessly integrated with SQLite, accompanied by Docker configuration and a Jenkins CI/CD pipeline.

## Project Overview

The **Express-SQLite** project aims to provide a straightforward example of building, containerizing, and automating a Node.js application using popular development and deployment tools. Below is an overview of the key components and their functionalities:

### 1. Application (app.js)

The core of the project is an Express.js application with SQLite database integration. The application exposes two endpoints:

- **GET /items:** Retrieve a list of items from the SQLite database.
- **POST /items:** Add a new item to the SQLite database.

The SQLite database (`mydatabase.db`) is initialized with a single table (`items`) during startup.

### 2. Dockerization

The project includes a Dockerfile for containerizing the Express.js application. The Dockerfile specifies the base Node.js image, sets the working directory, copies necessary files, installs dependencies, exposes port 3000, and defines the command to run the application.

### 3. Docker Compose

The `docker-compose.yml` file configures a Docker Compose setup, defining a service for the Express.js application. It ensures the application is connected to an SQLite database by mounting a volume for persistent data storage.

### 4. Jenkins CI/CD Pipeline

The Jenkins CI/CD pipeline automates the build, delivery, and cleanup processes. The pipeline, defined in the `Jenkinsfile`, includes the following stages:

- **Checkout:** Fetch the latest code from the repository.
- **Init:** Authenticate with DockerHub.
- **Build:** Build the Docker image with a unique tag.
- **Deliver:** Push the Docker image to DockerHub.
- **Cleanup:** Remove the local Docker image and logout from DockerHub.

## Getting Started

To set up and run the project locally, follow these general steps:

1. **Project Setup:**
   - Clone the repository.
  
2. **Express.js Application:**
   - Navigate to the project directory (`express-sqlite`).
   - Install dependencies (`npm install`).

3. **Dockerization:**
   - Build the Docker image (`docker build -t <username>/express-sqlite-app .`).
   - Test the Docker image locally (`docker run -p 3000:3000 <username>/express-sqlite-app`).
   - Push the Docker image to DockerHub (`docker push <username>/express-sqlite-app`).

4. **Docker Compose:**
   - Run the Docker Compose setup (`docker-compose up`).
   - Ensure both the application and the database function correctly.

5. **Jenkins CI/CD Pipeline:**
   - Add DockerHub credentials to Jenkins.
   - Commit and push changes to trigger the Jenkins pipeline.

## Notes

- Make sure to replace `<username>` with your DockerHub username in the Docker commands and configurations.
- Adjust the paths and configurations as needed for your specific setup.
- This README provides a basic overview; additional documentation and details may be needed based on your project requirements.