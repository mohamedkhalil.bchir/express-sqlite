pipeline {
    agent {
        docker {
            image 'your-docker-image:latest'
        }
    }

    environment {
        DOCKERHUB_CREDENTIALS = credentials('dh_cred')
        IMAGE_TAG = "${DOCKERHUB_CREDENTIALS_USR}/express-sqlite-app:${BUILD_ID}"
    }

    options {
        timestamps()
        disableConcurrentBuilds()
    }

    triggers {
        pollSCM('*/5 * * * *')
    }

    stages {
        stage('Checkout') {
            steps {
                checkout scm
            }
        }

        stage('Init') {
            steps {
                withCredentials([usernamePassword(credentialsId: 'dh_cred', usernameVariable: 'DOCKERHUB_CREDENTIALS_USR', passwordVariable: 'DOCKERHUB_CREDENTIALS_PSW')]) {
                    sh "echo \$DOCKERHUB_CREDENTIALS_PSW | docker login -u \$DOCKERHUB_CREDENTIALS_USR --password-stdin"
                }
            }
        }

        stage('Build') {
            steps {
                sh "docker build -t \$IMAGE_TAG ."
            }
        }

        stage('Deliver') {
            steps {
                sh "docker push \$IMAGE_TAG"
            }
        }

        stage('Cleanup') {
            steps {
                sh "docker rmi \$IMAGE_TAG"
                sh 'docker logout'
            }
        }
    }

    post {
        success {
            echo 'Deployment successful!'
        }
        failure {
            echo 'Deployment failed!'
        }
        always {
            script {
                sh 'docker logout'
            }
        }
    }
}
